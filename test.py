import time
import board
import busio
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)#usamos modo BCM para la numeracion pines
#--------------------ACTIVAR ELECTROVALVULAS----------------------------
# Defición pines de salidas los reles
GPIO.setup(11, GPIO.OUT, initial = 0)
GPIO.setup(5, GPIO.OUT, initial = 0)
GPIO.setup(6, GPIO.OUT, initial = 0)
GPIO.setup(13, GPIO.OUT, initial = 0)
GPIO.setup(19, GPIO.OUT, initial = 0)
GPIO.setup(26, GPIO.OUT, initial = 0)

# Crear el bus pa I2C 
i2c = busio.I2C(board.SCL, board.SDA)

# Crear el objeto ADC usando el bus I2C bus
#ads = ADS.ADS1115(i2c)
ads = ADS.ADS1115(i2c, address=0x48)

# Crear una entrada de un solo extremo en channel 0
chan = AnalogIn(ads, ADS.P0)
chan1 = AnalogIn(ads, ADS.P1)
chan2 = AnalogIn(ads, ADS.P2)
chan3 = AnalogIn(ads, ADS.P3)

# Crear una entrada diferencial entre channel 0 y 1
#chan = AnalogIn(ads, ADS.P0, ADS.P1)

print("{:>5}\t{:>5}".format("raw", "v"))

#while True:
    # print("{:>5}\t{:>5.3f}".format(chan.value, chan.voltage))
    #print("Chan:"+"{:>5}\t{:>5.3f}".format(chan.voltage, chan1.voltage))
    #print("Chan: "+"{:>5}\t{:>5.3f}".format(chan.voltage,chan1.voltage, chan2.voltage, chan3.voltage))    
    #time.sleep(0.5)

#---------------SENSORES DE HUMEDAD-------------------
#def medirHumedad():
       #if chan.voltage >= 2: 
       # print("Suelo seco")
       # GPIO.output(26,0)
        #print("Electrovalula1 encendida")
        
      #else:
       # print("Suelo Humedo")
      #  GPIO.output(26,1)
      #  print("Electrovalula1 apagada")
    
print("sensando humedad")
   
while True:
       print("Chan: "+"{:>5}\t{:>5.3f}".format(chan.voltage,chan1.voltage, chan2.voltage, chan3.voltage))    
  #     medirHumedad()
       time.sleep(1)